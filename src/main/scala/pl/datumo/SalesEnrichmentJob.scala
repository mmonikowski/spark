package pl.datumo

import org.apache.spark.sql.SparkSession
import pl.datumo.integration.spark.bigquery._

object SalesEnrichmentJob {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .config("spark.master", "local[*]")
      .config("spark.hadoop.fs.gs.project.id", "datumo-training")
      .config("spark.hadoop.bq.staging_dataset.location", "EU")
      .config("spark.hadoop.mapred.bq.project.id", "datumo-training")
      .config("spark.hadoop.mapred.bq.gcs.bucket", "datumo-training_temp")
      .config("google.cloud.auth.service.account.enable", "true")
      .config("google.cloud.auth.service.account.json.keyfile", "service_key/service_key.json")
      .appName("DatumoBottegaTraining")
      .getOrCreate()

    val query  = "SELECT * FROM sales.sales"

    val sales = spark.sqlContext.bigQuerySelect(query).repartition(5)

    sales.show(10)

    spark.stop()
  }
}